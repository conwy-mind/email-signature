const { src, dest } = require('gulp');

function buildTask(cb) {
	src('src/*.{png,html}')
		.pipe(dest('public'));

	cb();
}

exports.build = buildTask;